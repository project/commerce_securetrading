README file for Commerce Secure Trading

CONTENTS OF THIS FILE
---------------------
* Introduction
* Requirements
* Installation
* Configuration
* How It Works
* Troubleshooting
* Maintainers

INTRODUCTION
------------
This project integrates Secure Trading online payments into
the Drupal Commerce payment and checkout systems.
https://docs.trustpayments.com/


REQUIREMENTS
------------
This module requires the following:
* Submodules of Drupal Commerce package (https://drupal.org/project/commerce)
  - Commerce core
  - Commerce Payment (and its dependencies)
* Secure Trading PHP Library (https://github.com/SecureTrading/PHP-API)
* JWT Firebase library (https://github.com/firebase/php-jwt)


INSTALLATION
------------
* This module needs to be installed via Composer,
 which will download the required libraries.
`composer require "drupal/commerce_securetrading"`
 https://www.drupal.org/docs/8/extending-drupal-8/installing-modules-composer-dependencies

CONFIGURATION
-------------
* Create a new Secure Trading payment gateway.
  Administration > Commerce > Configuration >
  Payment gateways > Add payment gateway

  Secure Trading -specific settings are available:
  - Username (for use with Webservices API)
  - Password (for use with Webservices API)
  - JWT user (for use with JS library)
  - Secret key (for use with JS library)
  - Sitereference (your site reference visible on Myst dashboard)
  - Domain (choose based on your merchant account between EU and US)
  Use the API credentials provided by your Secure Trading account. It is
  recommended to enter test credentials and then override these with live
  credentials in settings.php. This way,
  live credentials will not be stored in the db.


HOW IT WORKS
------------

* General considerations:
  - The store owner must have a Secure Trading merchant account.
    Sign up here:
    https://www.trustpayments.com/contact-us/
  - Customers should have a valid credit card.
    - Secure Trading provides several dummy credit card numbers for testing:
      https://docs.trustpayments.com/document/testing/

* Checkout workflow:
  It follows the Drupal Commerce Credit Card workflow.
  The customer should enter his/her credit card data
  or select one of the credit cards saved with Secure Trading
  from a previous order.

  **Module supports default Commerce checkout workflow**
  Order information (Payment information pane) -> Review -> Payment

  **Module support default Secure Trading implementation:**
  Order information or Review (with Payment information pane) -> Payment
  (this workflow enables executing default behavior from Secure Trading where
   THREEDQUERY and AUTH are validate on same step )

* Payment Terminal
  The store owner can Void, Capture and Refund the Secure Trading payments.


TROUBLESHOOTING
---------------
* On stored payment methods for American Express credit cards,
  ST does not allow entering 4-digit cvv


MAINTAINERS
-----------
