<?php

/**
 * @file
 * Hooks provided by the Commerce Secure Trading module.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Let other modules respond to data sent for use with JS library.
 *
 * @param array $request_data
 *   Response data sent to JS trough Drupal API.
 *
 * @see \Drupal\commerce_securetrading\Plugin\Commerce\PaymentGateway\SecureTrading::prepareJwtRequestData
 * @see https://docs.trustpayments.com/document/javascript-library/extra-features/customise/iframes/
 */
function hook_commerce_securetrading_js_data_alter(array &$request_data) {
  $request_data['styles']['defaultStyles']['background-color-input'] = 'Black';
}

/**
 * Let other modules respond to data for JWT token.
 *
 * @param array $jwt_data
 *   Array of data used for generating JWT token.
 *
 * @see \Drupal\commerce_securetrading\Plugin\Commerce\PaymentGateway\SecureTrading::prepareJwtRequestData
 * @see https://docs.trustpayments.com/document/toolbox/toolbox-fields-that-can-be-submitted-in-the-payload/
 */
function hook_commerce_securetrading_jwt_data_alter(array &$jwt_data) {
  $jwt_data['payload']['customercountry'] = 'UK';
}

/**
 * @} End of "addtogroup hooks".
 */
