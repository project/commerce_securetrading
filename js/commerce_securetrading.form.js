/**
 * @file
 * Javascript to generate SecureTrading elements and token in PCI-compliant way.
 */

(function ($, Drupal, drupalSettings) {

  'use strict';

  Drupal.commerceSecureTrading = {
    displayError: function (errorMessage) {
      $('#payment-errors').html(Drupal.theme('commerceSecureTradingError', errorMessage));
    }
  }
  Drupal.theme.commerceSecureTradingError = function (message) {
    return $('<div class="messages messages--error"></div>').html(message);
  }

  /**
   * Attaches the commerceSecureTrading behavior.
   *
   * @type {Drupal~behavior}
   */
  Drupal.behaviors.commerceSecureTrading = {

    attach: function (context) {

      if (!drupalSettings.commerceSecureTrading || !drupalSettings.commerceSecureTrading.token) {
        return;
      }

      // See https://github.com/SecureTrading/js-payments/issues/1132
      if ($('.securetrading-form').length === 0 && $('iframe#st-control-frame-iframe').length > 0) {
        $('iframe#st-control-frame-iframe').remove();
      }

      $('.securetrading-form', context).once('securetrading-form-processed').each(function () {

        const $form = $(this).closest('form');
        if ($form.length === 0) {
          return;
        }

        const $form_id = $form.attr('id');
        const $submit = $form.find(':input.button--primary');
        const $submit_id = $submit.attr('id');

        const config = {
          jwt: drupalSettings.commerceSecureTrading.token,
          livestatus: drupalSettings.commerceSecureTrading.livestatus,
          formId: $form_id,
          buttonId: $submit_id,
          fieldsToSubmit: drupalSettings.commerceSecureTrading.fieldsToSubmit,
          placeholders: drupalSettings.commerceSecureTrading.placeholders,
          styles: drupalSettings.commerceSecureTrading.styles,
          deferInit: false
        };

        // Initialize SecureTrading.
        let st = SecureTrading(config);

        let components = {
          startOnLoad: false
        };

        if (drupalSettings.commerceSecureTrading.requestTypes != undefined) {
          components.requestTypes = drupalSettings.commerceSecureTrading.requestTypes;
        }

        // Attach components specifics.
        st.Components(components);

        if ($('#securetrading-method-id', $form).length > 0) {
          // If we need to update JWT.
          if ($('#securetrading-method-id', $form).val().length > 0) {
            st.updateJWT(
              drupalSettings.commerceSecureTrading.token
            );
          }

          // Set value, so we can react on switching to another payment method
          // to update JWT token if needed.
          $('#securetrading-method-id', $form).val(drupalSettings.commerceSecureTrading.method_id);
        }

        $form.on('submit.commerce_securetrading', function (e) {

          // Add specific handling when we have errors.
          // SecureTrading is set to not submit on error,
          // and handles display of them it self.
          // But credit card declines are needed to be handled
          // from our side.
          st.submitCallback = function(response) {

            if (response.errorcode !== "0") {
              let errorCode = response.errorcode;
              let errorMessage = response.errormessage;

              // Generic message from ST is just: Decline.
              if (errorCode === "70000") {
                errorMessage = Drupal.t('Your credit card has been declined');
              }

              Drupal.commerceSecureTrading.displayError(errorMessage);
            }
          };

          // On submit just proceed. Secure Trading is blocking
          // submit on errors.
          st.successCallback = function () {
            $form.submit();
          };
        });
      });
    },

    detach: function (context, settings, trigger) {
      if (trigger !== 'unload') {
        return;
      }

      const $form = $('.securetrading-form', context).closest('form');
      if ($form.length === 0) {
        return;
      }

      $form.off('submit.commerce_securetrading');
    }
  };

})(jQuery, Drupal, drupalSettings);
