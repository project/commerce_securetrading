<?php

namespace Drupal\commerce_securetrading;

use Drupal\profile\Entity\ProfileInterface;

/**
 * Class SecureTradingHelper.
 *
 * @package Drupal\commerce_securetrading
 */
class SecureTradingHelper {

  /**
   * List of available Secure Trading domains.
   */
  const SECURE_TRADING_DOMAINS = [
    'eu' => 'https://webservices.securetrading.net',
    'us' => 'https://webservices.securetrading.us',
  ];

  /**
   * Settle automatic, with fraud and duplicate checks.
   */
  const SECURE_TRADING_SETTLEMENTS_AUTOMATIC = '0';

  /**
   * Settle automatic, without fraud and duplicate checks.
   */
  const SECURE_TRADING_SETTLEMENTS_AUTOMATIC_NO_CHECK = '1';

  /**
   * Authorize transacation, but don't settle automatic.
   */
  const SECURE_TRADING_SETTLEMENTS_SUSPENDED = '2';

  /**
   * Cancel settlement.
   */
  const SECURE_TRADING_SETTLEMENTS_CANCEL = '3';

  /**
   * Acquire settlement from the bank.
   */
  const SECURE_TRADING_SETTLEMENTS_SETTLING = '10';

  // Generic flags, used for storing data in key value per order.
  const JWT_TOKEN_RESPONSE = 'jwt';
  const JWT_3D_RESPONSE = 'threedresponse';
  const JWT_SEPARATE_AUTH = 'auth_separate';

  // How long tokens could be stored for maximum amount of time.
  const JTW_EXPIRATION = 86400;

  /**
   * List of required keys.
   *
   * Keys which are going be posted directly on server,
   * in the format of an application/x-www-form-urlencoded POST.
   *
   * @return array
   *   Return list of keys.
   */
  public static function inputJwtValues() {
    return [
      'transactionreference',
      'errormessage',
      'jwt',
      'settlestatus',
      'errorcode',
      'orderreference',
    ];
  }

  /**
   * Format user address.
   *
   * @param \Drupal\profile\Entity\ProfileInterface|null $profile
   *   The user profile.
   *
   * @return array
   *   Return formatted address prepared for JS.
   */
  public static function mapBilling($profile) {
    $data = [];

    if ($profile instanceof ProfileInterface && !$profile->get('address')->isEmpty()) {
      /** @var \Drupal\address\Plugin\Field\FieldType\AddressItem $address */
      $address = $profile->get('address')->first();

      $data['billingfirstname'] = $address->getGivenName();
      $data['billinglastname'] = $address->getFamilyName();

      if (!empty($address->getAdministrativeArea())) {
        $data['billingcounty'] = $address->getAdministrativeArea();
      }

      $data['billingstreet'] = $address->getAddressLine1();

      if (!empty($address->getAddressLine2())) {
        $data['billingpremise'] = $address->getAddressLine2();
      }

      $data['billingtown'] = $address->getLocality();
      $data['billingcountry'] = $address->getCountryCode();
      $data['billingpostcode'] = $address->getPostalCode();
    }

    return $data;
  }

}
