<?php

namespace Drupal\commerce_securetrading\PluginForm\Onsite;

use Drupal\commerce_checkout\Plugin\Commerce\CheckoutFlow\CheckoutFlowInterface;
use Drupal\commerce_payment\PluginForm\PaymentMethodAddForm as BasePaymentMethodAddForm;
use Drupal\commerce_securetrading\SecureTradingHelper;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class PaymentMethodAddForm.
 *
 * @package Drupal\commerce_securetrading\PluginForm\Onsite
 */
class PaymentMethodAddForm extends BasePaymentMethodAddForm {

  /**
   * {@inheritdoc}
   */
  protected function buildCreditCardForm(array $element, FormStateInterface $form_state) {

    // Alter the form with Secure Trading class.
    $element['#attributes']['class'][] = 'securetrading-form';
    /** @var \Drupal\commerce_securetrading\Plugin\Commerce\PaymentGateway\SecureTradingInterface $plugin */
    $plugin = $this->plugin;

    /** @var \Drupal\commerce_payment\Entity\PaymentMethodInterface $payment_method */
    $payment_method = $this->entity;

    // Get checkout flow.
    $checkout_flow = $form_state->getFormObject();

    if (!$checkout_flow instanceof CheckoutFlowInterface) {
      $element['markup'] = [
        '#type' => 'markup',
        '#markup' => $this->t('It is not possible to create stored payment method without order attached'),
      ];
      return $element;
    }

    // Prepare data for use with JS library.
    $jwt_data = $plugin->prepareJwtRequestData($payment_method, $checkout_flow);

    $element['#attached']['drupalSettings']['commerceSecureTrading'] = $jwt_data;

    $element['card_number'] = [
      '#type' => 'item',
      '#title' => t('Card number'),
      '#title_display' => 'invisible',
      '#required' => TRUE,
      '#validated' => TRUE,
      '#markup' => '<div id="st-card-number" class="st-card-number"></div>',
    ];

    $element['expiration'] = [
      '#type' => 'item',
      '#title' => t('Expiration date'),
      '#title_display' => 'invisible',
      '#required' => TRUE,
      '#validated' => TRUE,
      '#markup' => '<div id="st-expiration-date" class="st-expiration-date"></div>',
    ];

    $element['security_code'] = [
      '#type' => 'item',
      '#title' => t('CVC'),
      '#title_display' => 'invisible',
      '#required' => TRUE,
      '#validated' => TRUE,
      '#markup' => '<div id="st-security-code" class="st-security-code"></div>',
    ];

    // To display validation errors from SecureTrading automatically.
    $element['st_notification_frame'] = [
      '#type' => 'markup',
      '#markup' => '<div id="st-notification-frame"></div>',
      '#weight' => -200,
    ];

    // To display specific validation errors trough Drupal JS.
    $element['payment_errors'] = [
      '#type' => 'markup',
      '#markup' => '<div id="payment-errors"></div>',
      '#weight' => 200,
    ];

    $cacheability = new CacheableMetadata();
    $cacheability->addCacheableDependency($this->entity);
    $cacheability->setCacheMaxAge(0);
    $cacheability->applyTo($element);
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  protected function validateCreditCardForm(array &$element, FormStateInterface $form_state) {
    // The JS library performs its own validation.
  }

  /**
   * {@inheritdoc}
   */
  public function submitCreditCardForm(array $element, FormStateInterface $form_state) {
    $payment_details = [];

    $input = $form_state->getUserInput();

    // Secure trading is doing post with JWT token and few other fields.
    $secure_trading_input = SecureTradingHelper::inputJwtValues();

    foreach ($secure_trading_input as $key) {
      if (isset($input[$key])) {
        $payment_details[$key] = $input[$key];
      }

      if (isset($input['threedresponse'])) {
        $payment_details['threedresponse'] = $input['threedresponse'];
      }
    }

    if (empty($payment_details['jwt'])) {
      $form_state->setError($element, 'Missing token. Please try again');
    }

    $form_state->setValue([
      'payment_information',
      'add_payment_method',
      'payment_details',
    ], $payment_details);

    // The payment gateway plugin will process the submitted payment details.
    $values = $form_state->getValues();
    if (!empty($values['contact_information']['email'])) {
      // Then we are dealing with anonymous user. Adding a customer email.
      $payment_details = $values['payment_information']['add_payment_method']['payment_details'];
      $payment_details['customer_email'] = $values['contact_information']['email'];
      $form_state->setValue([
        'payment_information',
        'add_payment_method',
        'payment_details',
      ], $payment_details);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    if (isset($form['billing_information'])) {
      $form['billing_information']['#after_build'][] = [
        get_class($this),
        'addAddressAttributes',
      ];
    }

    return $form;
  }

  /**
   * Element #after_build callback: adds "data-st-name" to address properties.
   *
   * This allows our JavaScript to pass these values to Secure Trading as
   * customer information, enabling CVC, Zip, and Street checks.
   *
   * @param array $element
   *   The form element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   The modified form element.
   */
  public static function addAddressAttributes(array $element, FormStateInterface $form_state) {
    if (isset($element['address'])) {
      $element['address']['widget'][0]['address']['given_name']['#attributes']['data-st-name'] = 'billingfirstname';
      $element['address']['widget'][0]['address']['family_name']['#attributes']['data-st-name'] = 'billinglastname';
      $element['address']['widget'][0]['address']['address_line1']['#attributes']['data-st-name'] = 'billingstreet';
      $element['address']['widget'][0]['address']['address_line2']['#attributes']['data-st-name'] = 'billingpremise';
      $element['address']['widget'][0]['address']['locality']['#attributes']['data-st-name'] = 'billingtown';
      $element['address']['widget'][0]['address']['postal_code']['#attributes']['data-st-name'] = 'billingpostcode';

      if (isset($element['address']['widget'][0]['address']['administrative_area'])) {
        $element['address']['widget'][0]['address']['administrative_area']['#attributes']['data-st-name'] = 'billingcounty';
      }
      // Country code is a sub-element and needs another callback.
      $element['address']['widget'][0]['address']['country_code']['#pre_render'][] = [get_called_class(), 'addCountryCodeAttributes'];
    }

    return $element;
  }

  /**
   * Element #pre_render callback: adds "data-st-name" to the country_code.
   *
   * This ensures data-st-name is on the hidden or select element for the
   * country code, so that it is properly passed to SecureTrading.
   *
   * @param array $element
   *   The form element.
   *
   * @return array
   *   The modified form element.
   */
  public static function addCountryCodeAttributes(array $element) {
    $element['country_code']['#attributes']['data-st-name'] = 'billingcountryiso2a';
    return $element;
  }

}
