<?php

namespace Drupal\commerce_securetrading\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_checkout\Plugin\Commerce\CheckoutFlow\CheckoutFlowInterface;
use Drupal\commerce_payment\CreditCard;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\Entity\PaymentMethodInterface;
use Drupal\commerce_payment\Exception\HardDeclineException;
use Drupal\commerce_payment\Exception\InvalidRequestException;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_payment\PaymentMethodTypeManager;
use Drupal\commerce_payment\PaymentTypeManager;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OnsitePaymentGatewayBase;
use Drupal\commerce_price\Price;
use Drupal\commerce_securetrading\SecureTradingHelper;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\KeyValueStore\KeyValueExpirableFactoryInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Firebase\JWT\JWT;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use function Securetrading\api;

/**
 * Provides the Secure Trading payment gateway.
 *
 * @CommercePaymentGateway(
 *   id = "securetrading",
 *   label = "Secure Trading",
 *   display_label = "Secure Trading",
 *   forms = {
 *     "add-payment-method" = "Drupal\commerce_securetrading\PluginForm\Onsite\PaymentMethodAddForm",
 *   },
 *   payment_method_types = {"credit_card"},
 *   credit_card_types = {
 *     "amex", "dinersclub", "discover", "jcb", "maestro", "mastercard", "visa",
 *   },
 *   js_library = "commerce_securetrading/form",
 *   requires_billing_information = FALSE,
 * )
 */
class SecureTrading extends OnsitePaymentGatewayBase implements SecureTradingInterface {

  /**
   * The route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The logger factory.
   *
   *@var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The key value expirable factory.
   *
   * @var \Drupal\Core\KeyValueStore\KeyValueExpirableFactoryInterface
   */
  protected $keyValueExpirable;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, PaymentTypeManager $payment_type_manager, PaymentMethodTypeManager $payment_method_type_manager, TimeInterface $time, RouteMatchInterface $route_match, ModuleHandlerInterface $module_handler, LoggerInterface $logger, KeyValueExpirableFactoryInterface $key_value_expirable) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager, $payment_type_manager, $payment_method_type_manager, $time);
    $this->routeMatch = $route_match;
    $this->logger = $logger;
    $this->moduleHandler = $module_handler;
    $this->keyValueExpirable = $key_value_expirable;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.commerce_payment_type'),
      $container->get('plugin.manager.commerce_payment_method_type'),
      $container->get('datetime.time'),
      $container->get('current_route_match'),
      $container->get('module_handler'),
      $container->get('logger.channel.commerce_payment'),
      $container->get('keyvalue.expirable')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'sitereference' => '',
      'username' => '',
      'password' => '',
      'jwt_user' => '',
      'secret_key' => '',
      'domain' => '',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['username'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Username'),
      '#description' => $this->t('User with webservices role, used for backend operation for refund, void and capture'),
      '#default_value' => $this->configuration['username'],
      '#required' => TRUE,
    ];

    $form['password'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Password'),
      '#default_value' => $this->configuration['password'],
      '#required' => TRUE,
    ];

    $form['jwt_user'] = [
      '#type' => 'textfield',
      '#title' => $this->t('JWT user'),
      '#description' => $this->t('User with JWT role, used for JS and JWT requests'),
      '#default_value' => $this->configuration['jwt_user'],
      '#required' => TRUE,
    ];

    $form['secret_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Secret key'),
      '#default_value' => $this->configuration['secret_key'],
      '#required' => TRUE,
    ];

    $form['sitereference'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Site reference'),
      '#default_value' => $this->configuration['sitereference'],
      '#required' => TRUE,
    ];

    $form['domain'] = [
      '#type' => 'radios',
      '#options' => [
        'eu' => 'European Gateway',
        'us' => 'US Gateway',
      ],
      '#title' => $this->t('Payment gateway domain'),
      '#default_value' => $this->configuration['domain'],
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      $this->configuration['username'] = $values['username'];
      $this->configuration['password'] = $values['password'];
      $this->configuration['jwt_user'] = $values['jwt_user'];
      $this->configuration['secret_key'] = $values['secret_key'];
      $this->configuration['sitereference'] = $values['sitereference'];
      $this->configuration['domain'] = $values['domain'];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getSecretKey() {
    return $this->configuration['secret_key'];
  }

  /**
   * {@inheritdoc}
   */
  public function getJwtUser() {
    return $this->configuration['jwt_user'];
  }

  /**
   * {@inheritdoc}
   */
  public function getUsername() {
    return $this->configuration['username'];
  }

  /**
   * {@inheritdoc}
   */
  public function getPassword() {
    return $this->configuration['password'];
  }

  /**
   * {@inheritdoc}
   */
  public function getSiteReference() {
    return $this->configuration['sitereference'];
  }

  /**
   * {@inheritdoc}
   */
  public function getDomainLibrary() {
    return 'commerce_securetrading/' . $this->configuration['domain'];
  }

  /**
   * {@inheritdoc}
   */
  public function getDomain() {
    return SecureTradingHelper::SECURE_TRADING_DOMAINS[$this->configuration['domain']];
  }

  /**
   * {@inheritdoc}
   */
  public function getJsLibrary() {
    return $this->getDomainLibrary();
  }

  /**
   * {@inheritdoc}
   */
  public function createPayment(PaymentInterface $payment, $capture = TRUE) {
    $this->assertPaymentState($payment, ['new']);
    $payment_method = $payment->getPaymentMethod();
    $this->assertPaymentMethod($payment_method);

    $order_id = $payment->getOrderId();
    $token_storage = $this->keyValueExpirable->get('commerce_order_' . $order_id);

    $jwt_token = $token_storage->get(SecureTradingHelper::JWT_TOKEN_RESPONSE);
    $response = $this->decodeJwtResponse($jwt_token);
    $remote_id = $response->transactionreference ?? $payment_method->getRemoteId();

    $separate_auth = $token_storage->get(SecureTradingHelper::JWT_SEPARATE_AUTH);

    if (!empty($separate_auth)) {
      $request = [
        'orderreference' => $payment->getOrderId(),
        'parenttransactionreference' => $response->transactionreference,
        'sitereference' => $this->getSiteReference(),
        'requesttypedescription' => 'AUTH',
      ];

      // If we have threedresponse saved, means we are doing manually auth0
      // authentification.
      $threedresponse = $token_storage->get(SecureTradingHelper::JWT_3D_RESPONSE);

      if (!empty($threedresponse)) {
        $request['threedresponse'] = $threedresponse;
      }

      // Process request.
      $auth_response = $this->processSecureTradingResponse($request);

      // Set new id.
      if (isset($auth_response['responses'][0]['transactionreference'])) {
        // Set new remote id.
        $remote_id = $auth_response['responses'][0]['transactionreference'];
      }
    }

    $next_state = $capture ? 'completed' : 'authorization';
    $payment->setState($next_state);
    $payment->setRemoteId($remote_id);
    $payment->save();

    // Handle cases where we made 3D auth only, we need to push
    // new remote id and set method to be reusable.
    if (!empty($separate_auth)) {
      $payment_method->setRemoteId($remote_id);
      $payment_method->setReusable(TRUE);
      $payment_method->save();
    }

    // Delete saved tokens in key value.
    // We don't need these anymore.
    $token_storage->deleteAll();
  }

  /**
   * {@inheritdoc}
   */
  public function capturePayment(PaymentInterface $payment, Price $amount = NULL) {
    $this->assertPaymentState($payment, ['authorization']);
    // If not specified, capture the entire amount.
    $amount = $amount ?: $payment->getAmount();

    $request_data = [
      'requesttypedescriptions' => ['TRANSACTIONUPDATE'],
      'filter' => [
        'sitereference' => [['value' => $this->getSiteReference()]],
        'transactionreference' => [['value' => $payment->getRemoteId()]],
        'settlebaseamount' => [['value' => $this->formatMinorUnits($amount)]],
      ],
      'updates' => ['settlestatus' => SecureTradingHelper::SECURE_TRADING_SETTLEMENTS_AUTOMATIC_NO_CHECK],
    ];

    // SecureTrading embed exceptions and returns always response.
    $this->processSecureTradingResponse($request_data);

    $payment->setState('completed');
    $payment->setAmount($amount);
    $payment->save();
  }

  /**
   * {@inheritdoc}
   */
  public function voidPayment(PaymentInterface $payment) {
    $this->assertPaymentState($payment, ['authorization']);

    $request_data = [
      'requesttypedescriptions' => ['TRANSACTIONUPDATE'],
      'filter' => [
        'sitereference' => [['value' => $this->getSiteReference()]],
        'transactionreference' => [['value' => $payment->getRemoteId()]],
      ],
      'updates' => ['settlestatus' => SecureTradingHelper::SECURE_TRADING_SETTLEMENTS_CANCEL],
    ];

    // SecureTrading embed exceptions and returns always response.
    $this->processSecureTradingResponse($request_data);

    $payment->setState('authorization_voided');
    $payment->save();
  }

  /**
   * {@inheritdoc}
   */
  public function refundPayment(PaymentInterface $payment, Price $amount = NULL) {
    $this->assertPaymentState($payment, ['completed', 'partially_refunded']);
    // If not specified, refund the entire amount.
    $amount = $amount ?: $payment->getAmount();
    $this->assertRefundAmount($payment, $amount);

    $request_data = [
      'requesttypedescriptions' => ['REFUND'],
      'sitereference' => $this->getSiteReference(),
      'parenttransactionreference' => $payment->getRemoteId(),
      'baseamount' => $this->formatMinorUnits($amount),
    ];

    // Process request.
    $this->processSecureTradingResponse($request_data);

    $old_refunded_amount = $payment->getRefundedAmount();
    $new_refunded_amount = $old_refunded_amount->add($amount);
    if ($new_refunded_amount->lessThan($payment->getAmount())) {
      $payment->setState('partially_refunded');
    }
    else {
      $payment->setState('refunded');
    }

    $payment->setRefundedAmount($new_refunded_amount);
    $payment->save();
  }

  /**
   * {@inheritdoc}
   */
  public function createPaymentMethod(PaymentMethodInterface $payment_method, array $payment_details) {
    // The expected keys are payment gateway specific.
    // They are expected to be valid.
    $required_keys = SecureTradingHelper::inputJwtValues();

    foreach ($required_keys as $required_key) {
      if (!isset($payment_details[$required_key])) {
        throw new InvalidRequestException(sprintf('$payment_details must contain the %s key.', $required_key));
      }
    }

    $reusable = TRUE;

    $jwt_token = $payment_details['jwt'];
    $response = $this->decodeJwtResponse($jwt_token);

    // Remote API needs a remote transaction, not customer.
    // On saved payment method we need previous authorized transaction
    // reference.
    $remote_id = $response->transactionreference;

    // Fetch data from remote.
    // There is expiry date on remote, but not on local JWT response.
    $request_data = [
      'requesttypedescriptions' => ['TRANSACTIONQUERY'],
      'filter' => [
        'sitereference' => [['value' => $this->getSiteReference()]],
        'transactionreference' => [['value' => $response->transactionreference]],
      ],
    ];

    // SecureTrading embed exceptions and returns always response.
    $remote_transaction = $this->processSecureTradingResponse($request_data);

    if (isset($remote_transaction['responses'][0]['records'][0]['expirydate'])) {
      $dates = explode('/', $remote_transaction['responses'][0]['records'][0]['expirydate']);
      [$month, $year] = $dates;

      $payment_method->card_exp_month = $month;
      $payment_method->card_exp_year = $year;
      $expires = CreditCard::calculateExpirationTimestamp($month, $year);
      $payment_method->setExpiresTime($expires);
    }

    // Set card type based on credit card mapping.
    $payment_method->card_type = $this->mapCreditCardType($response->paymenttypedescription);
    // Only the last 4 numbers are safe to store.
    $payment_method->card_number = substr($response->maskedpan, -4);
    $payment_method->setRemoteId($remote_id);

    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    if ($order = $this->routeMatch->getParameter('commerce_order')) {

      $order_id = $order->id();
      $token_storage = $this->keyValueExpirable->get('commerce_order_' . $order_id);

      // If we do 3D auth only, we need to have this saved.
      $token_storage->setWithExpire(SecureTradingHelper::JWT_TOKEN_RESPONSE, $jwt_token, SecureTradingHelper::JTW_EXPIRATION);

      // Cleanup old ones.
      if (empty($payment_details['threedresponse']) && !empty($token_storage->get(SecureTradingHelper::JWT_3D_RESPONSE))) {
        $token_storage->delete(SecureTradingHelper::JWT_3D_RESPONSE);
      }

      // Set threedresponse and separate auth flag.
      if ($response->requesttypedescription === 'THREEDQUERY') {
        if (!empty($payment_details['threedresponse'])) {
          $token_storage->setWithExpire(SecureTradingHelper::JWT_3D_RESPONSE, $payment_details['threedresponse'], SecureTradingHelper::JTW_EXPIRATION);
        }

        // Mark that we need to do manually AUTH.
        $token_storage->setWithExpire(SecureTradingHelper::JWT_SEPARATE_AUTH, TRUE, SecureTradingHelper::JTW_EXPIRATION);

        // Mark initially method as not reusable while we only made 3D AUTH,
        // and user is still in checkout.
        $reusable = FALSE;
      }
    }

    $payment_method->setReusable($reusable);
    $payment_method->save();
  }

  /**
   * {@inheritdoc}
   */
  public function deletePaymentMethod(PaymentMethodInterface $payment_method) {
    // Delete locally entry only. On remote does not exist entity
    // which represents payment method. We just reuse old payment transaction
    // reference for tokenization.
    $payment_method->delete();
  }

  /**
   * {@inheritdoc}
   */
  public function updatePaymentMethod(PaymentMethodInterface $payment_method) {
    // Do nothing.
  }

  /**
   * {@inheritdoc}
   */
  public function prepareJwtRequestData(PaymentMethodInterface $payment_method, $checkout_flow, $parent_transaction = FALSE) {
    $jwt_data = [
      'iat' => time(),
      'iss' => $this->getJwtUser(),
      'payload' => [
        'accounttypedescription' => 'ECOM',
        'sitereference' => $this->getSiteReference(),
      ],
    ];

    $billing = SecureTradingHelper::mapBilling($payment_method->getBillingProfile());

    $jwt_data['payload'] += $billing;

    // Check order data.
    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    if ($order = $this->routeMatch->getParameter('commerce_order')) {
      $total_price = $order->getTotalPrice();

      $jwt_data['payload']['currencyiso3a'] = $total_price->getCurrencyCode();
      $jwt_data['payload']['mainamount'] = (string) round($total_price->getNumber(), 2);
      $jwt_data['payload']['orderreference'] = $order->id();

      if (!empty($order->getEmail())) {
        $jwt_data['payload']['billingemail'] = $order->getEmail();
      }

      // Send billing data if payment methods does not have it yet.
      if (empty($billing) && ($order_billing = SecureTradingHelper::mapBilling($order->getBillingProfile()))) {
        $jwt_data['payload'] += $order_billing;
      }
    }

    // Default requests.
    $request_types = [];

    $fields_to_submit = ['pan', 'expirydate', 'securitycode'];

    // Checkout flow specific data.
    if ($checkout_flow instanceof CheckoutFlowInterface) {
      $capture = $checkout_flow->getConfiguration()['panes']['payment_process']['capture'] ? 'automatic' : 'manual';

      // Set only for manual. If nothing is set, default is automatic.
      if ($capture === 'manual') {
        $jwt_data['payload']['settlestatus'] = SecureTradingHelper::SECURE_TRADING_SETTLEMENTS_SUSPENDED;
      }
    }

    if ($parent_transaction) {
      $jwt_data['payload']['parenttransactionreference'] = $payment_method->getRemoteId();
      $fields_to_submit = ['securitycode'];
    }

    else {
      $current_step = $this->routeMatch->getParameter('step');
      $next_step = $checkout_flow->getNextStepId($current_step);

      // If we have new payment method, which is in default commerce
      // checkout flow, we need to make 3D first.
      // then later auth on last page.
      if ($next_step !== 'payment') {
        $request_types = ['THREEDQUERY'];
      }
    }

    // React on jwt data before token is created.
    // E.g sending shipping data.
    $this->moduleHandler->alter('commerce_securetrading_jwt_data', $jwt_data);

    // Generate JWT token.
    $token = JWT::encode($jwt_data, $this->getSecretKey());

    // Prepare request data.
    $request_data = [
      'livestatus' => (int) ($this->getMode() === 'live'),
      'fieldsToSubmit' => $fields_to_submit,
      'method_id' => $parent_transaction ? $payment_method->id() : 'new',
      'styles' => [
        'defaultStyles' => [
          'background-color-input' => 'White',
        ],
        'cardNumber' => [
          'font-size-input' => '1.5rem',
          'line-height-input' => '1.6rem',
        ],
        'expirationDate' => [
          'font-size-input' => '1.5rem',
          'line-height-input' => '1.6rem',
        ],
        'securityCode' => [
          'font-size-input' => '1.5rem',
          'line-height-input' => '1.6rem',
        ],
        'controlFrame' => [
          'color-error' => '#3358FF',
        ],
      ],
      'placeholders' => [
        'pan' => 'Card number',
        'expirydate' => 'MM/YY',
        'securitycode' => 'CVV',
      ],
    ];

    if (!empty($request_types)) {
      $request_data['requestTypes'] = $request_types;
    }

    // Allow reacting on styles.
    $this->moduleHandler->alter('commerce_securetrading_js_data', $request_data);

    // After alteration attach JWT token to array, to avoid tampering
    // or errors during hook for altering data for JS.
    $request_data['token'] = $token;

    return $request_data;
  }

  /**
   * Maps the SecureTrading credit card type to a Commerce credit card type.
   *
   * @param string $card_type
   *   The SecureTrading credit card type.
   *
   * @return string
   *   The Commerce credit card type.
   *
   * @see https://docs.trustpayments.com/document/javascript-library/extra-features/card-payment-methods/
   */
  protected function mapCreditCardType($card_type) {
    $map = [
      'AMEX' => 'amex',
      'DINERS' => 'dinersclub',
      'DISCOVER' => 'discover',
      'JCB' => 'jcb',
      'MAESTRO' => 'maestro',
      'MASTERCARD' => 'mastercard',
      'MASTERCARDDEBIT' => 'mastercard',
      'VISA' => 'visa',
      'DELTA' => 'visa',
      'ELECTRON' => 'visa',
      'PURCHASING' => 'visa',
      'VPAY' => 'visa',
    ];
    if (!isset($map[$card_type])) {
      throw new HardDeclineException(sprintf('Unsupported credit card type "%s".', $card_type));
    }

    return $map[$card_type];
  }

  /**
   * Format price unit according to SecureTrading requirement.
   *
   * @param \Drupal\commerce_price\Price $amount
   *   Price which units needs to be formatted.
   *
   * @return string
   *   Return formatted amount.
   */
  protected function formatMinorUnits(Price $amount) {
    return (string) round($this->toMinorUnits($amount), 0);
  }

  /**
   * Helper for decoding JWT token content.
   *
   * @param string $jwt_token
   *   JWT token.
   *
   * @return mixed
   *   Return decoded content from JWT token.
   */
  protected function decodeJwtResponse($jwt_token) {
    if (empty($jwt_token)) {
      throw new \InvalidArgumentException('We encountered an unexpected error processing your payment. Please try again later.');
    }
    // Decode JWT data. Set HS256, so that JWT lib does not complain.
    $jwt_data = JWT::decode($jwt_token, $this->getSecretKey(), ['HS256']);

    $response = end($jwt_data->payload->response);

    if (!empty($response->errorcode)) {
      throw new \InvalidArgumentException($response->errordata);
    }

    return $response;
  }

  /**
   * Initialize SecureTrading webservices API call.
   *
   * @return mixed
   *   Return SC API call.
   */
  protected function apiSecureTrading() {
    $configData = [
      'username' => $this->getUsername(),
      'password' => $this->getPassword(),
    ];

    return api($configData);
  }

  /**
   * Generic helper for processing SecureTrading response data.
   *
   * @param array $request_data
   *   Request data from SecureTrading Webservices API response.
   *
   * @return array
   *   Return SC response.
   */
  protected function processSecureTradingResponse(array $request_data) {
    // SecureTrading embed exceptions and returns always response.
    $response = $this->apiSecureTrading()->process($request_data);
    $response = $response->toArray();

    // If we got 0, it is successful request.
    // Otherwise throw generic error.
    if (!empty($response['responses'][0]['errorcode'])) {
      $error_message = $response['responses'][0]['errormessage'];

      // Write log entry.
      $this->logger->warning($error_message . ' @error_data', ['@error_data' => $response['responses'][0]['errordata']]);
      // Add specific error for card declines.
      if ($response['responses'][0]['errorcode'] === '70000') {
        $this->messenger()->addWarning($this->t('You credit card has been declined'));
      }
      throw new PaymentGatewayException($error_message);
    }

    return $response;
  }

}
