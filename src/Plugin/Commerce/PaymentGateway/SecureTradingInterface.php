<?php

namespace Drupal\commerce_securetrading\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_payment\Entity\PaymentMethodInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OnsitePaymentGatewayInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\SupportsAuthorizationsInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\SupportsRefundsInterface;

/**
 * Provides the interface for the Secure Trading payment gateway.
 */
interface SecureTradingInterface extends OnsitePaymentGatewayInterface, SupportsAuthorizationsInterface, SupportsRefundsInterface {

  /**
   * Get the Secure Trading secret key.
   *
   * @return string
   *   The Secure Trading secret key.
   */
  public function getSecretKey();

  /**
   * Get the Secure Trading site reference.
   *
   * @return string
   *   The Secure Trading site reference.
   */
  public function getSiteReference();

  /**
   * Get the allowed Secure Trading domain.
   *
   * @return string
   *   The Secure trading domain.
   */
  public function getDomain();

  /**
   * Get by domain Drupal defined library for domain.
   *
   * @return string
   *   Return library name for use on checkout.
   */
  public function getDomainLibrary();

  /**
   * Get the Secure Trading username.
   *
   * @return string
   *   Return username.
   */
  public function getUsername();

  /**
   * Get the Secure Trading password.
   *
   * @return string
   *   Return username.
   */
  public function getPassword();

  /**
   * Get the Secure Trading user for JWT requests.
   *
   * @return string
   *   Return username.
   */
  public function getJwtUser();

  /**
   * Helper for creating data with use for JS library.
   *
   * @param \Drupal\commerce_payment\Entity\PaymentMethodInterface $payment_method
   *   The payment method.
   * @param \Drupal\commerce_checkout\Plugin\Commerce\CheckoutFlow\CheckoutFlowInterface|null $checkout_flow
   *   The checkout flow plugin.
   * @param bool $parent_transaction
   *   If we use stored payment methods.
   *
   * @return array
   *   Return formatted data for attaching to JS.
   */
  public function prepareJwtRequestData(PaymentMethodInterface $payment_method, $checkout_flow, $parent_transaction = FALSE);

}
