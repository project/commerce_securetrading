<?php

namespace Drupal\commerce_securetrading\EventSubscriber;

use Drupal\commerce_checkout\CheckoutOrderManagerInterface;
use Drupal\commerce_order\Event\OrderEvent;
use Drupal\commerce_order\Event\OrderEvents;
use Drupal\commerce_payment\Entity\PaymentGatewayInterface;
use Drupal\commerce_securetrading\Plugin\Commerce\PaymentGateway\SecureTradingInterface;
use Drupal\commerce_securetrading\SecureTradingHelper;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\KeyValueStore\KeyValueExpirableFactoryInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Subscribes to events to synchronize orders with proper transaction reference.
 */
class OrderPaymentSubscriber implements EventSubscriberInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The checkout order manager.
   *
   * @var \Drupal\commerce_checkout\CheckoutOrderManagerInterface
   */
  protected $checkoutOrderManager;

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * The current request.
   *
   * @var \Symfony\Component\HttpFoundation\Request|null
   */
  protected $request;

  /**
   * The key value expirable factory.
   *
   * @var \Drupal\Core\KeyValueStore\KeyValueExpirableFactoryInterface
   */
  protected $keyValueExpirable;

  /**
   * Constructs a new OrderEventsSubscriber object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\commerce_checkout\CheckoutOrderManagerInterface $checkout_order_manager
   *   The checkout order manager.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   * @param \Drupal\Core\KeyValueStore\KeyValueExpirableFactoryInterface $key_value_expirable
   *   The key value expirable factory.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, CheckoutOrderManagerInterface $checkout_order_manager, RequestStack $request_stack, KeyValueExpirableFactoryInterface $key_value_expirable) {
    $this->entityTypeManager = $entity_type_manager;
    $this->checkoutOrderManager = $checkout_order_manager;
    $this->requestStack = $request_stack;
    $this->request = $request_stack->getCurrentRequest();
    $this->keyValueExpirable = $key_value_expirable;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      OrderEvents::ORDER_UPDATE => 'onOrderUpdate',
    ];
  }

  /**
   * Ensures that order have jwt token saved.
   *
   * @param \Drupal\commerce_order\Event\OrderEvent $event
   *   The event.
   */
  public function onOrderUpdate(OrderEvent $event) {
    $order = $event->getOrder();

    if (!$order) {
      return;
    }

    if (!empty($order->get('cart')->getString()) || $order->getState()->getId() != 'draft') {
      return;
    }

    $gateway = $order->get('payment_gateway');

    if ($gateway->isEmpty() || !$gateway->entity instanceof PaymentGatewayInterface) {
      return;
    }
    $plugin = $gateway->entity->getPlugin();
    if (!$plugin instanceof SecureTradingInterface) {
      return;
    }

    $current_step = $this->checkoutOrderManager->getCheckoutStepId($order);

    // On payment step ensure that there is JWT token saved
    // if is not present - in case of single step auth.
    if ($current_step === 'payment') {
      $token_storage = $this->keyValueExpirable->get('commerce_order_' . $order->id());

      if (empty($token_storage->get(SecureTradingHelper::JWT_TOKEN_RESPONSE)) && !empty($this->request->get('jwt'))) {
        $token_storage->setWithExpire(SecureTradingHelper::JWT_TOKEN_RESPONSE, $this->request->get('jwt'), SecureTradingHelper::JTW_EXPIRATION);
      }
    }
  }

}
