<?php

/**
 * @file
 * Contains commerce_securetrading.module.
 */

use Drupal\commerce_securetrading\SecureTradingHelper;
use Drupal\Core\Form\FormStateInterface;

/**
 * Implements hook_form_BASE_FORM_ID_alter().
 */
function commerce_securetrading_form_commerce_checkout_flow_alter(&$form, FormStateInterface $form_state, $form_id) {
  // Check if there is payment information pane and next step is
  // payment process.
  if (isset($form['payment_information']['payment_method'])) {

    // Populated by the JS library.
    // Use to distinct if we need to update JWT token or not.
    $form['securetrading_method_id'] = [
      '#type' => 'hidden',
      '#attributes' => [
        'id' => 'securetrading-method-id',
      ],
    ];
    $payment_method = $form['payment_information']['payment_method'];
    $stored_payment_method_id = is_numeric($payment_method['#default_value']) && !empty($payment_method['#default_value']) ? $payment_method['#default_value'] : NULL;

    // We have selected saved payment method.
    if ($stored_payment_method_id) {

      /** @var Drupal\commerce_checkout\Plugin\Commerce\CheckoutFlow\MultistepDefault $checkout_flow */
      $checkout_flow = $form_state->getFormObject();

      /** @var \Drupal\commerce_payment\Entity\PaymentMethodInterface $payment_method */
      $payment_method = \Drupal::entityTypeManager()->getStorage('commerce_payment_method')->load($stored_payment_method_id);

      /** @var \Drupal\commerce_payment\Entity\PaymentGatewayInterface $payment_gateway */
      $payment_gateway = $payment_method->getPaymentGateway();

      // Only proceed if we have securetrading plugin.
      if ($payment_gateway->getPluginId() === 'securetrading') {

        $current_step = \Drupal::routeMatch()->getParameter('step');
        $next_step = $checkout_flow->getNextStepId($current_step);

        if ($next_step === 'payment') {
          // Add class for JS library.
          $form['payment_information']['#attributes']['class'][] = 'securetrading-form';

          /** @var \Drupal\commerce_securetrading\Plugin\Commerce\PaymentGateway\SecureTradingInterface $plugin */
          $plugin = $payment_gateway->getPlugin();

          $form['#attached']['library'][] = $plugin->getDomainLibrary();

          // Add fields for security code and notifications.
          $fields_markup = '<div id="st-security-code" class="st-security-code"></div><div id="st-notification-frame"></div>';
          $form['payment_information']['payment_method']['#options'][$stored_payment_method_id] .= $fields_markup;

          // Generate JWT token and attach it.
          $jwt_data = $plugin->prepareJwtRequestData($payment_method, $checkout_flow, TRUE);
          $form['payment_information']['#attached']['drupalSettings']['commerceSecureTrading'] = $jwt_data;
        }

        // Our custom validation handler run's last, and save jwt token
        // in order.
        $form['#validate'][] = 'commerce_securetrading_order_jwt_handler_validate';

      }
    }
  }

  // Check if we are on review step, and there is no payment pane.
  elseif (!empty($form['#step_id']) && $form['#step_id'] === 'review') {
    /** @var Drupal\commerce_checkout\Plugin\Commerce\CheckoutFlow\MultistepDefault $checkout_flow */
    $checkout_flow = $form_state->getFormObject();
    $order = $checkout_flow->getOrder();

    if (!$order->payment_method->isEmpty()) {
      /** @var \Drupal\commerce_payment\Entity\PaymentMethodInterface $payment_method */
      $payment_method = $order->payment_method->entity;
      /** @var \Drupal\commerce_payment\Entity\PaymentGatewayInterface $payment_gateway */
      $payment_gateway = $order->payment_gateway->entity;

      if ($payment_gateway->getPluginId() === 'securetrading') {
        /** @var \Drupal\commerce_securetrading\Plugin\Commerce\PaymentGateway\SecureTradingInterface $plugin */
        $plugin = $payment_gateway->getPlugin();

        $form['#attached']['library'][] = $plugin->getDomainLibrary();

        // Add class for JS library.
        $form['review']['payment_information']['#attributes']['class'][] = 'securetrading-form';

        // Get key value storage per order id.
        $key_value_id = 'commerce_order_' . $order->id();
        $token_collection = \Drupal::keyValueExpirable($key_value_id);

        if (empty($token_collection->get(SecureTradingHelper::JWT_TOKEN_RESPONSE)) && empty($token_collection->get(SecureTradingHelper::JWT_SEPARATE_AUTH))) {
          // Add fields for security code and notifications.
          $fields_markup = '<div id="st-security-code" class="st-security-code"></div><div id="st-notification-frame"></div>';
          $form['#validate'][] = 'commerce_securetrading_order_jwt_handler_validate';
          $form['review']['payment_information']['#suffix'] = $fields_markup;

          // Generate JWT token and attach it.
          $jwt_data = $plugin->prepareJwtRequestData($payment_method, $checkout_flow, TRUE);
          $form['#attached']['drupalSettings']['commerceSecureTrading'] = $jwt_data;
        }
      }
    }
  }
}

/**
 * Validation handler when we have stored method and next step is payment.
 */
function commerce_securetrading_order_jwt_handler_validate(&$form, FormStateInterface $form_state) {
  $triggering_element = $form_state->getTriggeringElement();
  $input = $form_state->getUserInput();

  // If submit button is triggered, and we have jwt value.
  if (isset($triggering_element['#type']) && $triggering_element['#type'] == 'submit') {
    $checkout_flow = $form_state->getFormObject();

    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $order = $checkout_flow->getOrder();

    // Get key value storage per order id.
    $key_value_id = 'commerce_order_' . $order->id();
    $token_collection = \Drupal::keyValueExpirable($key_value_id);

    if (!empty($input['jwt'])) {
      $token_collection->setWithExpire(SecureTradingHelper::JWT_TOKEN_RESPONSE, $input['jwt'], SecureTradingHelper::JTW_EXPIRATION);
    }

    else {

      /** @var \Drupal\commerce_payment\Entity\PaymentMethodInterface $payment_method */
      $payment_method = \Drupal::entityTypeManager()->getStorage('commerce_payment_method')->load($input['payment_information']['payment_method']);

      /** @var \Drupal\commerce_payment\Entity\PaymentGatewayInterface $payment_gateway */
      $payment_gateway = $payment_method->getPaymentGateway();

      // We need to keep same data, if user goes back to method selection
      // and select existing one which is not fully completed, only 3D
      // authorized.
      $leave_3d_flag = !$payment_method->isReusable() && $payment_gateway->getPluginId() === 'securetrading';

      // Remove jwt token. If we make 3D auth on new method,
      // go to the review, and back and choose new method, we need to clear
      // data.
      if (!empty($token_collection->get(SecureTradingHelper::JWT_TOKEN_RESPONSE)) && !$leave_3d_flag) {
        $token_collection->deleteAll();
      }
    }
  }
}

/**
 * Implements hook_form_BASE_FORM_ID_alter().
 */
function commerce_securetrading_form_commerce_payment_method_add_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  /** @var \Drupal\commerce_payment\PluginForm\PaymentGatewayFormInterface $payment_gateway_form */
  $payment_gateway_form = $form['payment_method']['#inline_form'];

  if ($payment_gateway_form->getEntity()->getPaymentGateway()->getPluginId() === 'securetrading') {
    $form['actions']['submit']['#access'] = FALSE;
  }
}
